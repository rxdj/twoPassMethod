#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include "twoPass.h"

using namespace std;


cv::Scalar getRandomColor()
{
	uchar r = 255 * (rand() / (1.0 + RAND_MAX));
	uchar g = 255 * (rand() / (1.0 + RAND_MAX));
	uchar b = 255 * (rand() / (1.0 + RAND_MAX));
	return cv::Scalar(b, g, r);
}

cv::Mat showColorLabel(cv::Mat label)
{
	int imgW = label.cols;
	int imgH = label.rows;
	std::map<int, cv::Scalar> colors;

	cv::Mat colorLabel = cv::Mat::zeros(imgH, imgW, CV_8UC3);
	int *pLabel = (int*)label.data;
	uchar *pColorLabel = colorLabel.data;
	for (int i = 0; i < imgH; i++)
	{
		for (int j = 0; j < imgW; j++)
		{
			int idx = (i*imgW + j) * 3;
			int pixelValue = pLabel[i*imgW + j];
			if (pixelValue > 0)
			{
				if (colors.count(pixelValue) <= 0)
				{
					colors[pixelValue] = getRandomColor();
				}
				cv::Scalar color = colors[pixelValue];
				pColorLabel[idx + 0] = color[0];
				pColorLabel[idx + 1] = color[1];
				pColorLabel[idx + 2] = color[2];
			}
		}
	}

	return colorLabel;
}

void main()
{
	// 加载图像
	string imageName = "data/opencv.png";
	cv::Mat image = cv::imread(imageName, 1);
	if (!image.data)
	{
		cout << "No image data" << endl;
		getchar();
	}
	//转为灰度图
	cv::cvtColor(image, image, CV_RGB2GRAY);
	//阈值化，情景为255，背景为0
	cv::Mat threshImg;
	cv::threshold(image, threshImg, 200, 255, cv::THRESH_BINARY);
	cv::bitwise_not(threshImg, threshImg);

	cv::imshow("threshImg", threshImg);

	//连通域检测 two Pass方法标记图像
	cv::Mat labelImg=cv::Mat::zeros(image.size(), CV_32SC1);
	int maxLabel = -1;
	TwoPass twoPassMethod(threshImg.cols*threshImg.rows);
	twoPassMethod.run(threshImg, labelImg, maxLabel);

	cv::imshow("labelImg", labelImg * 60);

	//每个区域涂不同颜色
	cv::Mat colorLabelImg = showColorLabel(labelImg);

	cv::imshow("colorLabel", colorLabelImg);

	cv::waitKey(0);


}